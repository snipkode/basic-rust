fn main() {
    
    //Variable and Mutability
    let immutable_variable = 5;
    let mut mutable_variable = 16;
    mutable_variable +=4;

    println!("Immutable Variable {}", immutable_variable);
    println!("Mutable Variable {}", mutable_variable);

    let boolean: bool = true;
    let integer: i32 = 56; // Unsigned Number 32 Bytes
    let float: f64 = 3.14; // Floating-Number 64 Bytes
    let character: char = 'A';
    let string: &str = "Hello World!";

    // Data Types 
    println!("Boolean Data type : {}", boolean);
    println!("Integer Data type : {}", integer);
    println!("Floating Data type : {}", float);
    println!("Character Data Type : {}", character);
    println!("String Data Type : {}", string);

    // Conditional Structures
    let result = if boolean {
        "It's true"
    } else {
        "It's false"
    };

    println!("Conditional Structures Result :  {}", result);

    // Basic Syntax
    for number in 1..=10{
        println!("Loop iteration range {}", number);
    }

    let fruits = ["apple", "banana", "cherry"];
    for fruit in fruits.iter() {
        println!("Fruit : {}", fruit);
    }

    let mut num = 0;
    while num < 3 {
        println!("While Loop Iteration {}", num);
        num += 1;
    }

}
