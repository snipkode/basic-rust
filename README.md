## Quick Start Development

- Download Cargo Watch Package

```shell
cargo install cargo-watch
```

- Serving Watch Development

```shell
cargo watch -q -c -w src/ -x run
```